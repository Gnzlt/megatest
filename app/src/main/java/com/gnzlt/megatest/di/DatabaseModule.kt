package com.gnzlt.megatest.di

import android.content.Context
import androidx.room.Room
import com.gnzlt.megatest.data.MegaDatabase
import com.gnzlt.megatest.data.dao.CategoryDao
import com.gnzlt.megatest.data.dao.TransactionDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context): MegaDatabase =
        Room.databaseBuilder(context, MegaDatabase::class.java, "megaDb").build()

    @Provides
    @Singleton
    fun provideTransactionDao(database: MegaDatabase): TransactionDao =
        database.transactionDao()

    @Provides
    @Singleton
    fun provideCategoryDao(database: MegaDatabase): CategoryDao =
        database.categoryDao()
}
