package com.gnzlt.megatest.di

import android.content.Context
import com.gnzlt.megatest.home.HomeActivity
import com.gnzlt.megatest.home.categories.CategoriesFragment
import com.gnzlt.megatest.home.overview.OverviewFragment
import com.gnzlt.megatest.home.transactions.TransactionsFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ViewModelModule::class,
        DatabaseModule::class,
        NetworkModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(context: Context): Builder

        fun build(): AppComponent
    }

    fun inject(activity: HomeActivity)
    fun inject(overviewFragment: OverviewFragment)
    fun inject(transactionsFragment: TransactionsFragment)
    fun inject(categoriesFragment: CategoriesFragment)
}
