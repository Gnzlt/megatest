package com.gnzlt.megatest.home.transactions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.gnzlt.megatest.data.model.TransactionItem
import com.gnzlt.megatest.databinding.FragmentListBinding
import com.gnzlt.megatest.home.HomeActivity
import com.gnzlt.megatest.home.HomeViewModel
import javax.inject.Inject

class TransactionsFragment : Fragment() {

    companion object {
        const val TAG = "TransactionsFragment"

        @JvmStatic
        fun newInstance(): TransactionsFragment = TransactionsFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val homeViewModel: HomeViewModel by viewModels { viewModelFactory }
    private val transactionsAdapter: TransactionsAdapter by lazy { TransactionsAdapter(::openTransaction) }

    private lateinit var binding: FragmentListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (requireActivity() as HomeActivity).appComponent.inject(this)
        setupObservers()
    }

    private fun setupView() {
        binding.list.adapter = transactionsAdapter
        binding.btnAdd.setOnClickListener { openCreateTransaction() }
    }

    private fun setupObservers() {
        homeViewModel.onTransactions().observe(viewLifecycleOwner, ::showTransactions)
    }

    private fun showTransactions(list: List<TransactionItem>?) {
        transactionsAdapter.submitList(list)

        if (list == null) {
            showToast("There was an error")
        }
    }

    private fun openTransaction(transactionId: Long) {
        showToast("open $transactionId detail screen")
    }

    private fun openCreateTransaction() {
        homeViewModel.createTransaction()
    }

    private fun showToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }
}
