package com.gnzlt.megatest.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.gnzlt.megatest.MegaApp
import com.gnzlt.megatest.R
import com.gnzlt.megatest.databinding.ActivityHomeBinding
import com.gnzlt.megatest.di.AppComponent
import javax.inject.Inject

class HomeActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var appComponent: AppComponent
    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent = (applicationContext as MegaApp).appComponent.also { it.inject(this) }
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupView()
    }

    private fun setupView() {
        binding.viewpager.apply {
            adapter = HomePagerAdapter(this@HomeActivity)
            isUserInputEnabled = false
        }

        binding.navBottom.apply {
            selectedItemId = R.id.navigation_overview

            setOnNavigationItemSelectedListener { item ->
                binding.viewpager.setCurrentItem(item.order)
                return@setOnNavigationItemSelectedListener true
            }
        }
    }
}
