package com.gnzlt.megatest.home.transactions

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gnzlt.megatest.data.model.TransactionItem
import com.gnzlt.megatest.databinding.ItemTransactionBinding

class TransactionsAdapter(
    private val clickListener: (transactionId: Long) -> Unit
) : ListAdapter<TransactionItem, TransactionsViewHolder>(TransactionItem.DiffCallback()) {

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemTransactionBinding.inflate(layoutInflater, parent, false)

        return TransactionsViewHolder(binding).apply {
            binding.root.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION && adapterPosition < itemCount) {
                    clickListener(getItem(adapterPosition).id)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: TransactionsViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemId(position: Int): Long =
        getItem(position).id
}
