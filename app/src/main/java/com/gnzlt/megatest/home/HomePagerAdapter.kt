package com.gnzlt.megatest.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.gnzlt.megatest.home.HomePagerAdapter.Screen.CATEGORIES
import com.gnzlt.megatest.home.HomePagerAdapter.Screen.OVERVIEW
import com.gnzlt.megatest.home.HomePagerAdapter.Screen.TRANSACTIONS
import com.gnzlt.megatest.home.HomePagerAdapter.Screen.values
import com.gnzlt.megatest.home.categories.CategoriesFragment
import com.gnzlt.megatest.home.overview.OverviewFragment
import com.gnzlt.megatest.home.transactions.TransactionsFragment

class HomePagerAdapter(fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {

    enum class Screen { OVERVIEW, TRANSACTIONS, CATEGORIES }

    override fun createFragment(position: Int): Fragment =
        when (position) {
            OVERVIEW.ordinal -> OverviewFragment.newInstance()
            TRANSACTIONS.ordinal -> TransactionsFragment.newInstance()
            CATEGORIES.ordinal -> CategoriesFragment.newInstance()
            else -> throw error("Screen not found")
        }

    override fun getItemCount(): Int = values().size
}
