package com.gnzlt.megatest.home

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.gnzlt.megatest.data.model.CategoryItem
import com.gnzlt.megatest.data.model.TransactionItem
import com.gnzlt.megatest.data.usecase.CreateCategoryUseCase
import com.gnzlt.megatest.data.usecase.CreateTransactionUseCase
import com.gnzlt.megatest.data.usecase.GetCategoriesUseCase
import com.gnzlt.megatest.data.usecase.GetTransactionsUseCase
import com.gnzlt.megatest.data.usecase.GetUsdNzdRateUseCase
import com.gnzlt.megatest.util.notifyObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.Currency
import javax.inject.Inject

class HomeViewModel
@Inject constructor(
    private val getUsdNzdRateUseCase: GetUsdNzdRateUseCase,
    private val getTransactionsUseCase: GetTransactionsUseCase,
    private val getCategoriesUseCase: GetCategoriesUseCase,
    private val createTransactionUseCase: CreateTransactionUseCase,
    private val createCategoryUseCase: CreateCategoryUseCase
) : ViewModel() {

    companion object {
        private const val TAG = "HomeViewModel"
    }

    private val transactions: MutableLiveData<List<TransactionItem>?> = MutableLiveData()
    private val categories: MutableLiveData<List<CategoryItem>?> = MutableLiveData()
    private val exchangeRate: MutableLiveData<Double?> = MutableLiveData()

    init {
        getExchangeRate()
        getTransactions()
        getCategories()
    }

    fun onCategories(): LiveData<List<CategoryItem>?> = categories
    fun onTransactions(): LiveData<List<TransactionItem>?> = transactions.map { list ->
        val rate = exchangeRate.value
        if (rate != null && !list.isNullOrEmpty()) {
            val resultList = mutableListOf<TransactionItem>()
            list.forEach { item ->
                if (item.currency.currencyCode != "NZD") {
                    resultList.add(
                        TransactionItem(
                            item.id,
                            item.title,
                            item.categoryTitle,
                            item.categoryColor,
                            item.date,
                            item.amount,
                            item.currency,
                            item.amount * rate,
                            Currency.getInstance("USD")
                        )
                    )
                } else {
                    resultList.add(item)
                }
            }
            return@map resultList
        }
        list
    }

    @SuppressLint("CheckResult")
    private fun getExchangeRate() {
        getUsdNzdRateUseCase.run()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    exchangeRate.value = result
                    transactions.notifyObserver()
                },
                { throwable ->
                    Log.e(TAG, throwable.toString())
                    exchangeRate.value = null
                }
            )
    }

    @SuppressLint("CheckResult")
    private fun getTransactions() {
        getTransactionsUseCase.run()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    transactions.value = result
                },
                { throwable ->
                    Log.e(TAG, throwable.toString())
                    transactions.value = null
                }
            )
    }

    @SuppressLint("CheckResult")
    private fun getCategories() {
        getCategoriesUseCase.run()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    categories.value = result
                },
                { throwable ->
                    Log.e(TAG, throwable.toString())
                    categories.value = null
                }
            )
    }

    fun createTransaction() {
        createTransactionUseCase.runSample()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun createCategory() {
        createCategoryUseCase.runSample()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }
}
