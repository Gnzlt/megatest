package com.gnzlt.megatest.home.categories

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.gnzlt.megatest.data.model.CategoryItem
import com.gnzlt.megatest.databinding.ItemCategoryBinding

class CategoriesAdapter(
    private val clickListener: (categoryId: Long) -> Unit
) : ListAdapter<CategoryItem, CategoriesViewHolder>(CategoryItem.DiffCallback()) {

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemCategoryBinding.inflate(layoutInflater, parent, false)

        return CategoriesViewHolder(binding).apply {
            binding.root.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION && adapterPosition < itemCount) {
                    clickListener(getItem(adapterPosition).id)
                }
            }
        }
    }

    override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemId(position: Int): Long =
        getItem(position).id
}
