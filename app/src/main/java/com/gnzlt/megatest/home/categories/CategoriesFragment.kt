package com.gnzlt.megatest.home.categories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.gnzlt.megatest.data.model.CategoryItem
import com.gnzlt.megatest.databinding.FragmentListBinding
import com.gnzlt.megatest.home.HomeActivity
import com.gnzlt.megatest.home.HomeViewModel
import javax.inject.Inject

class CategoriesFragment : Fragment() {

    companion object {
        const val TAG = "CategoriesFragment"

        @JvmStatic
        fun newInstance(): CategoriesFragment = CategoriesFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val homeViewModel: HomeViewModel by viewModels { viewModelFactory }
    private val categoriesAdapter: CategoriesAdapter by lazy { CategoriesAdapter(::openCategory) }

    private lateinit var binding: FragmentListBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (requireActivity() as HomeActivity).appComponent.inject(this)
        setupObservers()
    }

    private fun setupView() {
        binding.list.adapter = categoriesAdapter
        binding.btnAdd.setOnClickListener { openCreateCategory() }
    }

    private fun setupObservers() {
        homeViewModel.onCategories().observe(viewLifecycleOwner, ::showCategories)
    }

    private fun showCategories(list: List<CategoryItem>?) {
        categoriesAdapter.submitList(list)

        if (list == null) {
            showToast("There was an error")
        }
    }

    private fun openCategory(categoryId: Long) {
        showToast("open $categoryId detail screen")
    }

    private fun openCreateCategory() {
        homeViewModel.createCategory()
    }

    private fun showToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }
}
