package com.gnzlt.megatest.home.categories

import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import com.gnzlt.megatest.data.model.CategoryItem
import com.gnzlt.megatest.databinding.ItemCategoryBinding

class CategoriesViewHolder(
    private val binding: ItemCategoryBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: CategoryItem) {
        binding.txtName.text = item.name
        binding.txtBudget.text = item.budget
        if (item.isBudgetExceeded) {
            binding.root.setBackgroundColor(Color.RED)
        }
    }
}
