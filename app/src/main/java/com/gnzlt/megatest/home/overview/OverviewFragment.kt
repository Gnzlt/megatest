package com.gnzlt.megatest.home.overview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.gnzlt.megatest.databinding.FragmentOverviewBinding
import com.gnzlt.megatest.home.HomeActivity
import com.gnzlt.megatest.home.HomeViewModel
import javax.inject.Inject

class OverviewFragment : Fragment() {

    companion object {
        const val TAG = "OverviewFragment"

        @JvmStatic
        fun newInstance(): OverviewFragment = OverviewFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val homeViewModel: HomeViewModel by viewModels { viewModelFactory }

    private lateinit var binding: FragmentOverviewBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentOverviewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (requireActivity() as HomeActivity).appComponent.inject(this)
        setupObservers()
    }

    private fun setupView() {
    }

    private fun setupObservers() {
    }
}
