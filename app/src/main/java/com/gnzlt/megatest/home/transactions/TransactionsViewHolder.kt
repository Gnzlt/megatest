package com.gnzlt.megatest.home.transactions

import androidx.core.graphics.toColorInt
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.gnzlt.megatest.data.model.TransactionItem
import com.gnzlt.megatest.databinding.ItemTransactionBinding
import java.text.NumberFormat
import java.util.Currency

class TransactionsViewHolder(
    private val binding: ItemTransactionBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: TransactionItem) {
        binding.root.setBackgroundColor(item.categoryColor.toColorInt())
        binding.txtTitle.text = item.title
        binding.txtDate.text = item.date
        binding.txtCategory.text = item.categoryTitle
        binding.txtPrice.text = getFormattedCurrency(item.amount, item.currency)

        if (item.exchangedAmount != null) {
            binding.txtExchange.text = getFormattedCurrency(item.exchangedAmount!!, item.exchangedCurrency!!)
            binding.txtExchange.isVisible = true
        } else {
            binding.txtExchange.isVisible = false
        }
    }

    private fun getFormattedCurrency(amount: Double, amountCurrency: Currency): String =
        NumberFormat.getCurrencyInstance()
            .apply { currency = amountCurrency }
            .format(amount)
}
