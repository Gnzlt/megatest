package com.gnzlt.megatest.data.usecase

import com.gnzlt.megatest.data.dao.CategoryDao
import com.gnzlt.megatest.data.model.CategoryItem
import io.reactivex.Flowable
import java.text.NumberFormat
import java.util.Currency
import javax.inject.Inject
import kotlin.random.Random

class GetCategoriesUseCase
@Inject constructor(
    private val categoriesDao: CategoryDao
) {

    fun run(): Flowable<List<CategoryItem>> =
        categoriesDao.getAll().map { list ->
            list.map { dbo ->
                CategoryItem(
                    dbo.id,
                    dbo.name,
                    getFormattedCurrency(dbo.budget, dbo.currencyCode),
                    Random.nextBoolean() // TODO
                )
            }
        }

    private fun getFormattedCurrency(amount: Double, currencyCode: String): String =
        NumberFormat.getCurrencyInstance()
            .apply { currency = Currency.getInstance(currencyCode) }
            .format(amount)
}
