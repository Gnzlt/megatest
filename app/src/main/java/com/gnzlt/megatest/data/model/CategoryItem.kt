package com.gnzlt.megatest.data.model

import androidx.recyclerview.widget.DiffUtil

data class CategoryItem(
    val id: Long,
    val name: String,
    val budget: String,
    val isBudgetExceeded: Boolean
) {

    class DiffCallback : DiffUtil.ItemCallback<CategoryItem>() {
        override fun areItemsTheSame(oldItem: CategoryItem, newItem: CategoryItem): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: CategoryItem, newItem: CategoryItem): Boolean =
            oldItem == newItem
    }
}
