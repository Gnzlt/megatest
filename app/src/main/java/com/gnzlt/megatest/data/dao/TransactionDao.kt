package com.gnzlt.megatest.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.gnzlt.megatest.data.model.local.TransactionDbo
import com.gnzlt.megatest.data.model.local.TransactionWithCategoryDbo
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface TransactionDao {

    @Query("SELECT * FROM transactions")
    fun getAll(): Flowable<List<TransactionDbo>>

    @Transaction
    @Query("SELECT * FROM transactions")
    fun getAllTransactionsWithCategory(): Flowable<List<TransactionWithCategoryDbo>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg transaction: TransactionDbo): Completable

    @Delete
    fun delete(transaction: TransactionDbo): Completable
}
