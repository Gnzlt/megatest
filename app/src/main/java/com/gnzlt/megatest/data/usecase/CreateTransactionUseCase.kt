package com.gnzlt.megatest.data.usecase

import com.gnzlt.megatest.data.dao.TransactionDao
import com.gnzlt.megatest.data.model.local.TransactionDbo
import io.reactivex.Completable
import java.util.Currency
import java.util.Date
import java.util.UUID
import javax.inject.Inject
import kotlin.random.Random

class CreateTransactionUseCase
@Inject constructor(
    private val transactionDao: TransactionDao
) {

    fun run(
        title: String,
        categoryId: Long,
        amount: Double,
        currency: Currency
    ): Completable {
        val dbo = TransactionDbo(
            id = UUID.randomUUID().leastSignificantBits,
            title = title,
            categoryId = categoryId,
            amount = amount,
            currencyCode = currency.currencyCode,
            date = Date().time
        )

        return transactionDao.insertAll(dbo)
    }

    fun runSample(): Completable =
        Completable.fromCallable {
            run(
                "Starbucks ${Random.nextInt(100)}",
                Random.nextLong(4),
                Random.nextDouble(500.0),
                if (Random.nextBoolean()) Currency.getInstance("USD") else Currency.getInstance("NZD")
            ).blockingAwait()
        }
}
