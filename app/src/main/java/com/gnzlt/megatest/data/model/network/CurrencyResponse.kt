package com.gnzlt.megatest.data.model.network

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class CurrencyResponse(
    @SerializedName("success") val success: Boolean,
    @SerializedName("quotes") val quotes: CurrencyDto
)
