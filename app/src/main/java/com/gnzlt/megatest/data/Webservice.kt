package com.gnzlt.megatest.data

import com.gnzlt.megatest.data.model.network.CurrencyResponse
import io.reactivex.Single
import retrofit2.http.GET

interface Webservice {

    @GET("/live?access_key=89082fd70b41673552f00cea734fb131&currencies=NZD&source=USD")
    fun getCurrency(): Single<CurrencyResponse>
}
