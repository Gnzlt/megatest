package com.gnzlt.megatest.data.usecase

import com.gnzlt.megatest.data.dao.TransactionDao
import com.gnzlt.megatest.data.model.TransactionItem
import io.reactivex.Flowable
import java.text.SimpleDateFormat
import java.util.Currency
import javax.inject.Inject

class GetTransactionsUseCase
@Inject constructor(
    private val transactionDao: TransactionDao
) {

    fun run(): Flowable<List<TransactionItem>> =
        transactionDao.getAllTransactionsWithCategory().map { list ->
            list.map { dbo ->
                TransactionItem(
                    dbo.transaction.id,
                    dbo.transaction.title,
                    dbo.category.name,
                    dbo.category.color,
                    getFormattedDate(dbo.transaction.date),
                    dbo.transaction.amount,
                    Currency.getInstance(dbo.transaction.currencyCode)
                )
            }
        }

    private fun getFormattedDate(timestamp: Long): String =
        SimpleDateFormat.getInstance().format(timestamp)
}
