package com.gnzlt.megatest.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gnzlt.megatest.data.dao.CategoryDao
import com.gnzlt.megatest.data.dao.TransactionDao
import com.gnzlt.megatest.data.model.local.CategoryDbo
import com.gnzlt.megatest.data.model.local.TransactionDbo

@Database(entities = [TransactionDbo::class, CategoryDbo::class], version = 1)
abstract class MegaDatabase : RoomDatabase() {

    abstract fun transactionDao(): TransactionDao

    abstract fun categoryDao(): CategoryDao
}
