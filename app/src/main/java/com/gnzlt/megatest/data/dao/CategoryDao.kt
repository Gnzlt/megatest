package com.gnzlt.megatest.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gnzlt.megatest.data.model.local.CategoryDbo
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface CategoryDao {

    @Query("SELECT * FROM categories")
    fun getAll(): Flowable<List<CategoryDbo>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg category: CategoryDbo): Completable

    @Delete
    fun delete(category: CategoryDbo): Completable
}
