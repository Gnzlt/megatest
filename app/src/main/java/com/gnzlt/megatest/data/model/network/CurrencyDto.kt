package com.gnzlt.megatest.data.model.network

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class CurrencyDto(
    @SerializedName("USDNZD") val value: Double
)
