package com.gnzlt.megatest.data.usecase

import com.gnzlt.megatest.data.dao.CategoryDao
import com.gnzlt.megatest.data.model.local.CategoryDbo
import io.reactivex.Completable
import java.util.Currency
import java.util.UUID
import javax.inject.Inject
import kotlin.random.Random

class CreateCategoryUseCase
@Inject constructor(
    private val categoryDao: CategoryDao
) {

    fun run(
        name: String,
        color: String,
        budget: Double,
        currency: Currency
    ): Completable {
        val dbo = CategoryDbo(
            id = UUID.randomUUID().leastSignificantBits,
            name = name,
            color = color,
            budget = budget,
            currencyCode = currency.currencyCode
        )

        return categoryDao.insertAll(dbo)
    }

    fun runSample(): Completable =
        Completable.fromCallable {
            val categories = listOf(
                CategoryDbo(0, "Drinks", "#94fd96", Random.nextDouble(300.0), "USD"),
                CategoryDbo(1, "Computers", "#41535a", Random.nextDouble(300.0), "NZD"),
                CategoryDbo(2, "Phones", "#4fa6bd", Random.nextDouble(300.0), "USD"),
                CategoryDbo(3, "Clothes", "#01783a", Random.nextDouble(300.0), "NZD"),
                CategoryDbo(4, "Food", "#ffc5f3", Random.nextDouble(300.0), "USD")
            )
            categoryDao.insertAll(*categories.toTypedArray()).blockingAwait()
        }
}
