package com.gnzlt.megatest.data.model

import androidx.recyclerview.widget.DiffUtil
import java.util.Currency

data class TransactionItem(
    val id: Long,
    val title: String,
    val categoryTitle: String,
    val categoryColor: String,
    val date: String,
    var amount: Double,
    var currency: Currency,
    var exchangedAmount: Double? = null,
    var exchangedCurrency: Currency? = null
) {

    class DiffCallback : DiffUtil.ItemCallback<TransactionItem>() {
        override fun areItemsTheSame(oldItem: TransactionItem, newItem: TransactionItem): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: TransactionItem, newItem: TransactionItem): Boolean =
            oldItem == newItem
    }
}
