package com.gnzlt.megatest.data.usecase

import com.gnzlt.megatest.data.Webservice
import io.reactivex.Single
import javax.inject.Inject

class GetUsdNzdRateUseCase
@Inject constructor(
    private val webservice: Webservice
) {

    fun run(): Single<Double> =
        webservice.getCurrency().map { it.quotes.value }
}
