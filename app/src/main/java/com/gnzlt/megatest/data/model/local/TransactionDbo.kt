package com.gnzlt.megatest.data.model.local

import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey

@Keep
@Entity(tableName = "transactions")
data class TransactionDbo(
    @PrimaryKey val id: Long,
    val title: String,
    val categoryId: Long,
    val amount: Double,
    val currencyCode: String,
    val date: Long
)
