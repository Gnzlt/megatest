package com.gnzlt.megatest.data.model.local

import androidx.annotation.Keep
import androidx.room.Embedded
import androidx.room.Relation

@Keep
data class TransactionWithCategoryDbo(
    @Embedded val transaction: TransactionDbo,
    @Relation(
        parentColumn = "categoryId",
        entityColumn = "id"
    )
    val category: CategoryDbo
)
