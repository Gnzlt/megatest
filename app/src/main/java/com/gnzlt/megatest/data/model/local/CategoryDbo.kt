package com.gnzlt.megatest.data.model.local

import androidx.annotation.Keep
import androidx.room.Entity
import androidx.room.PrimaryKey

@Keep
@Entity(tableName = "categories")
data class CategoryDbo(
    @PrimaryKey val id: Long,
    val name: String,
    val color: String,
    val budget: Double,
    val currencyCode: String
)
