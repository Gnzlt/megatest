package com.gnzlt.megatest

import android.app.Application
import com.gnzlt.megatest.di.AppComponent
import com.gnzlt.megatest.di.DaggerAppComponent

class MegaApp : Application() {

    val appComponent: AppComponent =
        DaggerAppComponent.builder().application(this).build()
}
